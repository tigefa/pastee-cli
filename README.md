Paste.ee CLI
============

[![Build Status](https://drone.meow.tf/api/badges/paste-ee/cli/status.svg)](https://drone.meow.tf/paste-ee/cli)

A simple, flexible CLI tool for uploading to Paste.ee.

Uploading Pastes
----------------

The CLI supports two methods of uploading, stdin and file arguments.

stdin:

```shell
echo hello | pastee
```

files:

```shell
pastee FILE1 FILE2 FILE3
```

Pastes can be encrypted by passing `-e` or `--encrypt` to the tool, which will encrypt on your server and upload just like the web interface.

Logging in
----------

If you'd like to attach your pastes to an account, you can login via `pastee login`. This will authenticate your account against the API and store an API key to upload with.
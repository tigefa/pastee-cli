PACKAGE_ARCH="$ARCH"

if [ "$PACKAGE_ARCH" = "arm" ]; then
  PACKAGE_ARCH="armhf"
elif [ "$PACKAGE_ARCH" = "386" ]; then
  PACKAGE_ARCH="i386"
fi

fpm -s dir -t deb -p /build/pastee_${DRONE_TAG}_${ARCH}.deb \
    -n pastee -v $DRONE_SEMVER -a $PACKAGE_ARCH \
    --deb-priority optional --force \
    --deb-compression gz --verbose \
    --description "Paste.ee CLI Tool" \
    -m "Paste.ee <admin@paste.ee>" --vendor "Paste.ee" \
    /build/pastee_linux_${ARCH}=/usr/bin/pastee

fpm -s dir -t rpm -p /build/pastee_${DRONE_TAG}_${ARCH}.rpm \
    -n pastee -v $DRONE_SEMVER -a $PACKAGE_ARCH \
    --description "Paste.ee CLI Tool" --verbose \
    -m "Paste.ee <admin@paste.ee>" --vendor "Paste.ee" \
    /build/pastee_linux_${ARCH}=/usr/bin/pastee